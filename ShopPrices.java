/**
 * The ShopPrices enum sets the price and currency for each item in
 * the ShopInterface. Prices are displayed using the ShopPurchases
 * class.
 *
 * @author: Anna Mae Decelo adecelo@myune.edu.au (Group: Just-A-Group)
 * @author: Alice Schofield aschof20@myune.edu.au (Group: Just-A-Group)
 */

package muck.client.Shop;

public enum ShopPrices {

    // Shop Hair Items
    FEMALE_SHOP_HAIR_BLANK(0,0, 0, ""),
    FEMALE_SHOP_HAIR_3(1,30, 1, "Chain Mail Helm"),
    FEMALE_SHOP_HAIR_13(2,20, 0, "Brunette Medium Hair"),
    FEMALE_SHOP_HAIR_23(23,20, 0, "Auburn Medium Hair"),
    FEMALE_SHOP_HAIR_12(3,20, 0, "Black Medium Hair"),
    FEMALE_SHOP_HAIR_14(4,80, 1, "Silver Helm"),
    FEMALE_SHOP_HAIR_15(5,30, 2, "Brunette Ponytail"),
    FEMALE_SHOP_HAIR_10(6,50, 0, "Green Ponytail"),
    FEMALE_SHOP_HAIR_11(7,50, 0, "Red Ponytail"),
    FEMALE_SHOP_HAIR_9(8,50, 0, "White Ponytail"),
    FEMALE_SHOP_HAIR_8(9, 80, 0, "Black Curly Ponytail"),
    FEMALE_SHOP_HAIR_21(10,80, 0, "Blonde Curly Ponytail"),
    FEMALE_SHOP_HAIR_22(11,80, 0, "Blue Curly Ponytail"),
    FEMALE_SHOP_HAIR_1(12,20,2, "Golden Helm"),
    FEMALE_SHOP_HAIR_19(13,80, 0, "Red Curly Ponytail"),
    FEMALE_SHOP_HAIR_18(14,60, 1, "Black Long Hair"),
    FEMALE_SHOP_HAIR_16(15,60, 1, "Green Long Hair"),
    FEMALE_SHOP_HAIR_17(16,60, 1, "Brown Long Hair"),
    FEMALE_SHOP_HAIR_20(17,60, 1, "Red Long Hair"),
    FEMALE_SHOP_HAIR_2(18,15, 0, "Cloth Hood"),
    FEMALE_SHOP_HAIR_4(19,30, 0, "Blue Long Bangs"),
    FEMALE_SHOP_HAIR_5(20,30, 0, "Blonde Long Bangs"),
    FEMALE_SHOP_HAIR_7(21,30, 0, "Grey Long Bangs"),
    FEMALE_SHOP_HAIR_6(22,30, 0, "Red Long Bangs"),
    FEMALE_SHOP_HAIR_24(24, 20, 0, "Blonde Medium Hair"),

    // Shop Female Tops
    FEMALE_TOP_BLANK(0,0, 0, ""),
    FEMALE_SHOP_TOP_14(1,5, 1, "Black Corset"),
    FEMALE_SHOP_TOP_1(2,10, 1, "Leather Armor"),
    FEMALE_SHOP_TOP_2(3,10, 2, "Silver Armor"),
    FEMALE_SHOP_TOP_4(4,10, 2, "Blue Dress with Blue Cape"),
    FEMALE_SHOP_TOP_3(5,5, 1, "Red Corset"),
    FEMALE_SHOP_TOP_8(6, 20, 2, "Red Corset with Wings"),
    FEMALE_SHOP_TOP_7(7,50, 0, "Brown Robe"),
    FEMALE_SHOP_TOP_5(8,10, 1, "Red Dress"),
    FEMALE_SHOP_TOP_6(9,10, 1, "White Dress"),
    FEMALE_SHOP_TOP_19(10,10, 2, "White Ball Gown"),
    FEMALE_SHOP_TOP_18(11,20, 2, "White Dress with Pink Wings"),
    FEMALE_SHOP_TOP_13(12,10, 2, "Black Dress with White Cape"),
    FEMALE_SHOP_TOP_20(13,30, 1, "Silver Robe with Blue Cape"),
    FEMALE_SHOP_TOP_21(14,20, 2, "Silver Robe with Wings"),
    FEMALE_SHOP_TOP_12(15,20, 2, "White Dress with Red Wings"),
    FEMALE_SHOP_TOP_15(16,15, 0, "Blue Tank Top"),
    FEMALE_SHOP_TOP_16(17,10, 2, "Brown Corset with Yellow Cape"),
    FEMALE_SHOP_TOP_17(18,20, 1, "Chainmail Top"),
    FEMALE_SHOP_TOP_11(19,20, 2, "Golden Armor"),
    FEMALE_SHOP_TOP_10(20,30, 1, "Green Dress"),

    // Shop Female Pants
    FEMALE_SHOP_PANTS_BLANK(0,0, 0, ""),
    FEMALE_SHOP_PANTS_12(1,30, 0, "Blue Pants with Grey Shoes"),
    FEMALE_SHOP_PANTS_3(2,30, 0, "Red Pants with Silver Shoes"),
    FEMALE_SHOP_PANTS_2(3,30, 0, "Pink Pants with White Shoes"),
    FEMALE_SHOP_PANTS_9(7,30, 0, "Blue Pants with Black Shoes"),
    FEMALE_SHOP_PANTS_7(5,30, 0, "White Pants with Black Shoes"),
    FEMALE_SHOP_PANTS_10(6,25, 0, "White Robe Skirt"),
    FEMALE_SHOP_PANTS_11(8,30, 1, "Blue Pants with Red Boots"),
    FEMALE_SHOP_PANTS_1(9,25, 0, "Robe Skirt with Black Shoes"),
    FEMALE_SHOP_PANTS_4(10,25, 0, "Robe Skirt with Brown Shoes"),
    FEMALE_SHOP_PANTS_8(11,10, 2, "Golden Greaves"),
    FEMALE_SHOP_PANTS_5(12,10, 2, "Silver Greaves"),

    // Shop Male Hair
    MALE_SHOP_HAIR_BLANK(0,0, 0, ""),
    MALE_SHOP_HAIR_14(1,80, 1, "Silver Helm"),
    MALE_SHOP_HAIR_13(2,60, 0, "Red Spiked"),
    MALE_SHOP_HAIR_12(3,60, 0, "Grey Spiked"),
    MALE_SHOP_HAIR_3(4,30, 1, "Chain Mail Helm"),
    MALE_SHOP_HAIR_9(5,10, 1, "Black Long Knot"),
    MALE_SHOP_HAIR_10(6,10, 1, "Red Long Knot"),
    MALE_SHOP_HAIR_11(7,10, 1, "Purple Long Knot"),
    MALE_SHOP_HAIR_15(8,10, 1, "Grey Long Knot"),
    MALE_SHOP_HAIR_22(9,30, 0, "Raven Long Bangs"),
    MALE_SHOP_HAIR_21(10, 30, 0, "Blonde Long Bangs"),
    MALE_SHOP_HAIR_8(11, 30, 0, "Black Long Bangs"),
    MALE_SHOP_HAIR_1(12,20, 2, "Golden Helm"),
    MALE_SHOP_HAIR_19(13,30, 0, "Purple Long Bangs"),
    MALE_SHOP_HAIR_18(14,5, 1, "Brunette Extra Long Hair"),
    MALE_SHOP_HAIR_16(15,5, 1, "Green Extra Long Hair"),
    MALE_SHOP_HAIR_17(16,5, 1, "Pink Extra Long Hair"),
    MALE_SHOP_HAIR_20(17,5, 1, "White Extra Long Hair"),
    MALE_SHOP_HAIR_2(18,15, 0, "Cloth Hood"),
    MALE_SHOP_HAIR_4(19,20, 0, "Black Bangs"),
    MALE_SHOP_HAIR_5(20,20, 0, "Blue Bangs"),
    MALE_SHOP_HAIR_7(21,20, 0, "Red Bangs"),
    MALE_SHOP_HAIR_6(22,20, 0, "White Bangs"),
    MALE_SHOP_HAIR_23(23,60, 0, "Raven Spiked"),
    MALE_SHOP_HAIR_24(24,60, 0, "Green Spiked"),

    // Shop Male Tops
    MALE_SHOP_TOP_BLANK(0,0, 0, ""),
    MALE_SHOP_TOP_13(1,10, 0, "Purple Tank Top"),
    MALE_SHOP_TOP_1(2,10, 0, "Red Tank Top with Belt"),
    MALE_SHOP_TOP_2(3,25, 1, "Blue Top with Wings"),
    MALE_SHOP_TOP_4(4,60, 0, "Button Up with Belt"),
    MALE_SHOP_TOP_3(5,40, 1, "Bow Tie Shirt"),
    MALE_SHOP_TOP_8(6,50, 1, "Black Tie Shirt"),
    MALE_SHOP_TOP_7(7,30, 1, "White Top with Wings"),
    MALE_SHOP_TOP_5(8,15, 1, "Purple Wings"),
    MALE_SHOP_TOP_12(12,10, 2, "Red Bow Tie Shirt with Vest"),
    MALE_SHOP_TOP_11(15,5, 0, "Brown Tank Top with Belt"),
    MALE_SHOP_TOP_15(16,10, 1, "Chainmail Top"),
    MALE_SHOP_TOP_14(17,30, 2, "Golden Armor"),
    MALE_SHOP_TOP_16(18,20, 1, "Chainmail Top with Green Vest"),
    MALE_SHOP_TOP_17(19,50, 1, "Leather Armor"),
    MALE_SHOP_TOP_9(21,30, 1, "Red Top with Wings"),

    // Shop Male Pants
    MALE_SHOP_PANTS_BLANK(0,0, 0, ""),
    MALE_SHOP_PANTS_12(1, 30, 0, "Red Pants with Black Shoes"),
    MALE_SHOP_PANTS_3(2,20, 2, "Golden Greaves"),
    MALE_SHOP_PANTS_2(3,10, 2, "Silver Greaves"),
    MALE_SHOP_PANTS_1(4,20, 0, "Robe Skirt"),
    MALE_SHOP_PANTS_4(5,30, 0, "Blue Pants with Black Shoes"),
    MALE_SHOP_PANTS_10(6,30, 0, "White Pants with Brown Shoes"),
    MALE_SHOP_PANTS_8(11,30, 0, "Blue Pants with Brown Shoes"),
    MALE_SHOP_PANTS_5(12,30, 0, "White Pants with Black Shoes");

    private final int index;
    private final int price;
    private final int currency;
    private final String itemName;

    ShopPrices(int index, int price, int currency, String itemName) {
        this.index = index;
        this.price = price;
        this.currency = currency;
        this.itemName = itemName;
    }
    public int toInt(){
        return index;
    }

    public int getShopPrice() {
        return price;
    }

    public int getShopCurrency() {
        return currency;
    }

    public String getItemName() { return itemName.toUpperCase(); }

}


