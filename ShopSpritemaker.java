package muck.client.Shop;

import muck.client.util.Util;

import java.io.File;
import java.util.Arrays;
/**
 * Class to create a ShopSpritemaker object.
 *
 * @author Alice Schofield aschof20@myune.edu.au
 * @author Anna Mae Decelo adecelo@myune.edu.au
 */
public class ShopSpritemaker {
    //Shop file arrays of single sprites and spritesheets.
    public static  File[] femaleShopHairSprite, maleShopHairSprite, femaleShopPantsSprite, maleShopPantsSprite,
            femaleShopTopSprite, maleShopTopSprite, femaleShopHairSpritesheet, maleShopHairSpritesheet,
            femaleShopPantsSpritesheet, maleShopPantsSpritesheet, femaleShopTopSpritesheet, maleShopTopSpritesheet;

    /**
     * Function to populate arrays of sprite features
     * @param files - files to populate the array
     */
    private static File[] populateFileArray(File files) {
        File[] fileSource = files.listFiles();
        assert fileSource != null;
        return arraySort(fileSource) ;
    }

    /**
     * Function to sort arrays so consistent results for different builds.
     * @param files - file array to be sorted.
     * @return      - array of sorted files.
     */
    private static File[] arraySort(File[] files){
        Arrays.sort(files);
        return files;
    }
    //Function to populate an array of images for each feature.
    public  void populateFeatureArrays() {
        //Shop stuff.
        File shopFemaleHair = Util.getResourceDirectory("/Shop/Features/Female/Hair");
        File shopMaleHair = Util.getResourceDirectory("/Shop/Features/Male/Hair");
        File shopFemalePants = Util.getResourceDirectory("/Shop/Features/Female/Bottoms");
        File shopMalePants = Util.getResourceDirectory("/Shop/Features/Male/Bottoms");
        File shopFemaleTops = Util.getResourceDirectory("/Shop/Features/Female/Tops");
        File shopMaleTops = Util.getResourceDirectory("/Shop/Features/Male/Tops");

        //Shop stuff.
        femaleShopHairSprite = populateFileArray(shopFemaleHair);
        maleShopHairSprite = populateFileArray(shopMaleHair);
        femaleShopPantsSprite = populateFileArray(shopFemalePants);
        maleShopPantsSprite = populateFileArray(shopMalePants);
        femaleShopTopSprite = populateFileArray(shopFemaleTops);
        maleShopTopSprite = populateFileArray(shopMaleTops);
    }

    //Populate an array of spritesheet images for each feature.
    public void populateSpritesheetFeatureArrays(){
        //Shop stuff.
        File shopFemaleTop = Util.getResourceDirectory("/Shop/Spritesheets/Female/Tops");
        File shopMaleTop = Util.getResourceDirectory("/Shop/Spritesheets/Male/Tops");
        File shopFemalePants = Util.getResourceDirectory("/Shop/Spritesheets/Female/Bottoms");
        File shopMalePants= Util.getResourceDirectory("/Shop/Spritesheets/Male/Bottoms");
        File shopFemaleHair = Util.getResourceDirectory("/Shop/Spritesheets/Female/Hair");
        File shopMaleHair = Util.getResourceDirectory("/Shop/Spritesheets/Male/Hair");

        //Shop stuff.
        femaleShopHairSpritesheet = populateFileArray(shopFemaleHair);
        maleShopHairSpritesheet = populateFileArray(shopMaleHair);
        femaleShopPantsSpritesheet = populateFileArray(shopFemalePants);
        maleShopPantsSpritesheet = populateFileArray(shopMalePants);
        femaleShopTopSpritesheet = populateFileArray(shopFemaleTop);
        maleShopTopSpritesheet = populateFileArray(shopMaleTop);
    }

    //Shop stuff.
    public File getMaleShopPants(int malePants){ return getFile(malePants, maleShopPantsSprite);}
    public File getFemaleShopPants(int femalePants){ return getFile(femalePants, femaleShopPantsSprite);}
    public File getMaleShopTop(int maleTop){ return getFile(maleTop, maleShopTopSprite);}
    public File getFemaleShopTop(int fTop){ return getFile(fTop, femaleShopTopSprite);}
    public File getMaleShopHair(int maleHair){ return getFile(maleHair, maleShopHairSprite);}
    public File getFemaleShopHair(int femaleHair){ return getFile(femaleHair, femaleShopHairSprite);}


    //Shop stuff.
    public File getFemaleShopPantsSpritesheet(int femalePants){ return getFile(femalePants, femaleShopPantsSpritesheet);}
    public File getMaleShopPantsSpritesheet(int malePants){ return getFile(malePants, maleShopPantsSpritesheet);}
    public File getFemaleShopTopSpritesheet(int femaleTops){ return getFile(femaleTops, femaleShopTopSpritesheet);}
    public File getMaleShopTopSpritesheet(int maleTops){ return getFile(maleTops, maleShopTopSpritesheet);}
    public File getFemaleShopHairSpritesheet(int femaleHair){ return getFile(femaleHair, femaleShopHairSpritesheet);}
    public File getMaleShopHairSpritesheet(int maleHair){ return getFile(maleHair, maleShopHairSpritesheet);}

    /**
     * Function to retieve a specific file based on array index.
     * @param feature - sprite feature to get.
     * @param array   - array populated with sprite features.
     * @return        - array.
     */
    private File getFile(int feature, File[] array){
        if(feature >= 0 && feature <= array.length){
            return array[feature];
        }
        return array[0];
    }
}
