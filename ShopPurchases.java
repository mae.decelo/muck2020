package muck.client.Shop;

import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import muck.economy.Balance;
import muck.economy.GameCurrency;
import java.util.stream.IntStream;

/**
 * The ShopPurchases class defines the methods for setting the displayed
 * prices of each item and the sum of prices for all the selected items
 * in the ShopInterface.
 *
 * Prices are set in the ShopPrices enum.
 *
 * @author: Anna Mae Decelo adecelo@myune.edu.au (Group: Just-A-Group)
 * @author: Alice Schofield aschof20@myune.edu.au (Group: Just-A-Group)
 */
public class ShopPurchases {
    // Arrays to store total price of items in each currency.
    // Index 0 = hair, index 1 = body, index 2 = legs.
    private static int[] totalCopperPrice = {0, 0, 0};
    private static int[] totalSilverPrice = {0, 0, 0};
    private static int[] totalGoldPrice = {0, 0, 0};

    /**
     * Function to update the total price displayed for the current items selected.
     * @param totalCopper - variable for copper total price.
     * @param totalSilver - variable for silver total price.
     * @param totalGold   - variable for gold total price.
     */
    private static void updateTotalPriceText(Text totalCopper, Text totalSilver, Text totalGold) {
        totalCopper.setText(String.valueOf(IntStream.of(totalCopperPrice).sum()));
        totalSilver.setText(String.valueOf(IntStream.of(totalSilverPrice).sum()));
        totalGold.setText(String.valueOf(IntStream.of(totalGoldPrice).sum()));
    }

    /**
     * Function to update the price for the most recent hair item selected.
     */
    static void updateHairPrice(ShopPrices hair, Text hairText, Text copperText,
                                Text silverText, Text goldText, Text hairName,
                                ImageView copper, ImageView silver, ImageView gold) {

        updatePurchaseTotal(hair, 0, hairText, hairName, copper, silver, gold);
        updateTotalPriceText(copperText, silverText, goldText);
    }

    /**
     * Function to update the price for the most recent top item selected.
     */
    static void updateTopPrice(ShopPrices top, Text topText, Text copperText,
                               Text silverText, Text goldText, Text topName,
                               ImageView copper, ImageView silver, ImageView gold) {

        updatePurchaseTotal(top, 1, topText, topName, copper, silver, gold);
        updateTotalPriceText(copperText, silverText, goldText);
    }

    /**
     * Funciton to update the price for the most recent pants item selected.
     */
    static void updatePantsPrice(ShopPrices pants, Text pantsText, Text copperText,
                                 Text silverText, Text goldText, Text pantsName,
                                 ImageView copper, ImageView silver, ImageView gold) {
        updatePurchaseTotal(pants, 2, pantsText, pantsName, copper, silver, gold);
        updateTotalPriceText(copperText, silverText, goldText);
    }

    /**
     * Function to reset the total coin prices.
     * @param index - array index to be reset.
     */
    private static void resetPrice(int index){
        totalCopperPrice[index] = 0;
        totalSilverPrice[index] = 0;
        totalGoldPrice[index] = 0;
    }

    /**
     * Function to reset all elements in the arrays.
     */
    public static void resetArrays() {
        for(int i = 0; i < 3; i++) {
            totalCopperPrice[i] = 0;
            totalSilverPrice[i] = 0;
            totalGoldPrice[i] = 0;
        }
    }

    /**
     * Function to update
     * @param itemPrice     - prices of the item selected.
     * @param coinIndex     - coins associated with the selected item.
     * @param itemText      - variable to display update in price.
     * @param itemName      - text of the item selected.
     * @param copperIcon    - Imageview to update copper coins.
     * @param silverIcon    - Imageview to update silver coins.
     * @param goldIcon      - Imageview to update gold coins.
     */
   private static void updatePurchaseTotal(ShopPrices itemPrice, int coinIndex, Text itemText, Text itemName,
                                           ImageView copperIcon, ImageView silverIcon, ImageView goldIcon){
       //Instantiate new balance.
       Balance balance = new Balance();

       // Reset total when new item selected before summation.
       resetPrice(coinIndex);

       balance.SetCurrency(GameCurrency.valueOf(itemPrice.getShopCurrency()), itemPrice.getShopPrice());
       int updatedPrice = balance.GetCurrency(GameCurrency.valueOf(itemPrice.getShopCurrency()));

       itemText.setText(String.valueOf(updatedPrice));
       itemName.setText(itemPrice.getItemName());

        if (itemPrice.getShopCurrency() == 0) {
            setCurrencyIcon(true, false, false, copperIcon, silverIcon, goldIcon);
            totalCopperPrice[coinIndex] = updatedPrice;
        } else if (itemPrice.getShopCurrency() == 1) {
            setCurrencyIcon(false, true, false, copperIcon, silverIcon, goldIcon);
            totalSilverPrice[coinIndex] = updatedPrice;
        } if (itemPrice.getShopCurrency() == 2) {
            setCurrencyIcon(false, false, true, copperIcon, silverIcon, goldIcon);
            totalGoldPrice[coinIndex] = updatedPrice;
        }
    }

    /**
     * Function to update the coin images displayed in the interface.
     * @param copper        - copper boolean.
     * @param silver        - silver boolean.
     * @param gold          - gold boolean.
     * @param copperIcon    - copper imageview to be updated.
     * @param silverIcon    - silver imageview to be updated.
     * @param goldIcon      - gold imageview to be updated.
     */
    private static void setCurrencyIcon(Boolean copper, Boolean silver, Boolean gold,
                                        ImageView copperIcon, ImageView silverIcon, ImageView goldIcon) {
        copperIcon.setVisible(copper);
        silverIcon.setVisible(silver);
        goldIcon.setVisible(gold);
    }

}
