package muck.client.Shop;

import javafx.scene.image.Image;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
/**
 * The ShopMaps create maps of the shop feature images.
 *
 * @author: Alice Schofield aschof20@myune.edu.au (Group: Just-A-Group)
 * @author: Anna Mae Decelo adecelo@myune.edu.au (Group: Just-A-Group)
 */
public class ShopMaps {

    public ShopMaps() {}

    private Map<Integer, Image> fShopHair = createFemaleShopHair();
    private Map<Integer, Image> mShopHair = createMaleShopHair();
    private Map<Integer, Image> fShopPants = createFemaleShopPants();
    private Map<Integer, Image> mShopPants = createMaleShopPants();
    private Map<Integer, Image> fShopTops = createFemaleShopTops();
    private Map<Integer, Image> mShopTops = createMaleShopTops();

    public Map<Integer, Image> getFemaleShopHair(){return fShopHair;}
    public Map<Integer, Image> getMaleShopHair(){return mShopHair;}
    public Map<Integer, Image> getFemaleShopTop(){return fShopTops;}
    public Map<Integer, Image> getMaleShopTop(){return mShopTops;}
    public Map<Integer, Image> getFemaleShopPants(){return fShopPants;}
    public Map<Integer, Image> getMaleShopPants(){return mShopPants;}

    // Shop Hair Items.
    private  Map<Integer, Image> createFemaleShopHair()  {
        Map<Integer, Image> femaleShopHairMap = new HashMap<>();
        ShopSpritemaker getSpriteFiles = new ShopSpritemaker();
        getSpriteFiles.populateFeatureArrays();
        populateMap( femaleShopHairMap, ShopSpritemaker.femaleShopHairSprite, "Female/Hair/");

        return femaleShopHairMap;
    }


    private Map<Integer, Image> createMaleShopHair() {
        Map<Integer, Image> maleShopHairMap = new HashMap<>();
        ShopSpritemaker getSpriteFiles = new ShopSpritemaker();
        getSpriteFiles.populateFeatureArrays();
        populateMap( maleShopHairMap, ShopSpritemaker.maleShopHairSprite, "Male/Hair/");

        return maleShopHairMap;
    }

    // Shop Top Items.
    private Map<Integer, Image> createFemaleShopTops()  {
        Map<Integer, Image> femaleShopTopMap = new HashMap<>();
        ShopSpritemaker getSpriteFiles = new ShopSpritemaker();
        getSpriteFiles.populateFeatureArrays();
        populateMap(femaleShopTopMap, ShopSpritemaker.femaleShopTopSprite, "Female/Tops/");

        return femaleShopTopMap;
    }

    private Map<Integer, Image> createMaleShopTops() {
        Map<Integer, Image> maleShopTopMap = new HashMap<>();
        ShopSpritemaker getSpriteFiles = new ShopSpritemaker();
        getSpriteFiles.populateFeatureArrays();
        populateMap(maleShopTopMap, ShopSpritemaker.maleShopTopSprite, "Male/Tops/");

        return maleShopTopMap;
    }

    // Shop Pants Items.
    private Map<Integer, Image> createFemaleShopPants(){
        Map<Integer, Image> femaleShopPantsMap = new HashMap<>();
        ShopSpritemaker getSpriteFiles = new ShopSpritemaker();
        getSpriteFiles.populateFeatureArrays();
        populateMap(femaleShopPantsMap, ShopSpritemaker.femaleShopPantsSprite, "Female/Bottoms/");

        return femaleShopPantsMap;
    }

    private Map<Integer, Image> createMaleShopPants() {
        Map<Integer, Image> maleShopPantsMap = new HashMap<>();
        ShopSpritemaker getSpriteFiles = new ShopSpritemaker();
        getSpriteFiles.populateFeatureArrays();
        populateMap(maleShopPantsMap, ShopSpritemaker.maleShopPantsSprite, "Male/Bottoms/");

           return maleShopPantsMap;
    }

    /**
     * Function to populate the maps
     * @param map - map to be created.
     * @param file - file of the image in the map.
     */
    private void populateMap(Map<Integer, Image> map, File[] file, String pathFragment) {
        ShopSpritemaker getSpriteFiles = new ShopSpritemaker();
        getSpriteFiles.populateFeatureArrays();
        for(int i = 0; i < file.length; i++){
            // Get pathname to of the image files.
            String pathName = "Shop/Features/" + pathFragment + file[i].getName();
            // Populate the hashmap
            map.put(i, new Image(pathName , 300, 300, true, false));
        }
    }
}
