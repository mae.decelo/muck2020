package muck.client.Shop;

import muck.economy.Balance;
import muck.economy.GameCurrency;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShopCurrencyTest {

    private static final Logger logger = LogManager.getLogger(ShopCurrencyTest.class);

    int itemCurrency = ShopPrices.FEMALE_SHOP_HAIR_1.getShopCurrency();
    int itemPrice = ShopPrices.FEMALE_SHOP_HAIR_1.getShopPrice();

    Balance balance = new Balance();

    // Test to check the correct prices are displayed using
    // the Balance class currency method.
    // (Disabled to fix broken build #232 11-Sept - Luke Dart) - Fixed 11-Sept Anna Decelo
    @Test
    void testItemPrice() {

        logger.info("Testing price and currency set in ShopPrices class.");
        balance.SetCurrency(GameCurrency.valueOf(itemCurrency), itemPrice);

        int price = balance.GetCurrency(GameCurrency.valueOf(itemCurrency));
        String currency = String.valueOf(GameCurrency.valueOf(itemCurrency));

        boolean testPrice = (price == 20);
        boolean testCurrency = (currency == "GOLD");

        assertAll(
                "Testing correct retrieval of item price and currency.",
                () -> assertTrue(testPrice, "Shop Female Hair Item 1 price is 80."),
                () -> assertTrue(testCurrency, "Shop Female Hair Item 1 currency is Gold.")
        );

    }

    // Test the price validation against user balance.
    @Test
    void testPriceValidation() {
        // Set a tester user balance.
        Balance userSetTestBalance = new Balance(new int[] {5, 10, 15});

        logger.info("Testing price validation against user balance.");

        balance.SetCurrency(GameCurrency.valueOf(itemCurrency), itemPrice);

        int price = balance.GetCurrency(GameCurrency.valueOf(itemCurrency));
        int userGetBalance = userSetTestBalance.GetCurrency(GameCurrency.valueOf(itemCurrency));

        boolean testUserBalance = (price > userGetBalance);

        assertTrue(testUserBalance, "Item price should be greater than the user balance.");
    }
}
