package muck.client.Shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
/**
 * ShopSpritemakerTest Class.
 * @author Alice Schofield aschof20@myune.edu.au
 */
class ShopSpritemakerTest {

    ShopSpritemaker testShopMaker = new ShopSpritemaker();

    /* Test female shop spritesheet getters */
    @Test
    void testFemaleSpriteMakerShopSpritesheetGetters() {
        testShopMaker.populateSpritesheetFeatureArrays();
        assertEquals("Hair1.png", testShopMaker.getFemaleShopHairSpritesheet(1).getName());
        assertEquals("Bottoms1.png", testShopMaker.getFemaleShopPantsSpritesheet(1).getName());
        assertEquals("Top1.png", testShopMaker.getFemaleShopTopSpritesheet(1).getName());
    }

    /* Test male shop spritesheet getters */
    @Test
    void testMaleSpriteMakerShopSpritesheetGetters() {
        testShopMaker.populateSpritesheetFeatureArrays();
        assertEquals("Hair1.png", testShopMaker.getMaleShopHairSpritesheet(1).getName());
        assertEquals("Bottoms1.png", testShopMaker.getMaleShopPantsSpritesheet(1).getName());
        assertEquals("Top1.png", testShopMaker.getMaleShopTopSpritesheet(1).getName());
    }

    /* Test female single shop getters */
    @Test
    void testSpriteMakerFemaleShopGetters() {
        testShopMaker.populateFeatureArrays();
        assertEquals("Hair1.png", testShopMaker.getFemaleShopHair(1).getName());
        assertEquals("Bottoms1.png", testShopMaker.getFemaleShopPants(1).getName());
        assertEquals("Top1.png", testShopMaker.getFemaleShopTop(1).getName());
    }

    /* Test male single shop getters */
    @Test
    void testSpriteMakerMaleShopGetters() {
        testShopMaker.populateFeatureArrays();
        assertEquals("Hair1.png", testShopMaker.getMaleShopHair(1).getName());
        assertEquals("Bottoms1.png", testShopMaker.getMaleShopPants(1).getName());
        assertEquals("Top1.png", testShopMaker.getMaleShopTop(1).getName());
    }
}