package muck.client.Shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShopPricesTest {

    /* Test of the index retrieval of shop items.*/
    @Test
    void toIntTest(){
        assertEquals(ShopPrices.FEMALE_SHOP_HAIR_1.toInt(), 12 );
    }

    /* Test of the getter for retrieval of shop item name.*/
    @Test
    void getItemNameTest(){
        assertEquals(ShopPrices.FEMALE_SHOP_HAIR_1.getItemName(), "GOLDEN HELM" );
    }
}