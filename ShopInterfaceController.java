package muck.client.Shop;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import muck.client.App;
import muck.client.CharacterBuilder.Features.Gender;
import muck.client.CharacterBuilder.Features.Hair;
import muck.client.CharacterBuilder.Features.Pants;
import muck.client.CharacterBuilder.Features.Tops;
import muck.client.CharacterBuilder.Sprite;
import muck.client.CharacterBuilder.SpriteMaps;
import muck.client.Inventory.InventoryTable;
import muck.client.MuckClient;
import muck.economy.Balance;
import muck.economy.GameCurrency;
import muck.protocol.connection.profile.RequestUpdateSprite;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

import static muck.client.Inventory.InventoryTable.getInventory;
import static muck.client.Inventory.InventoryTable.getInventoryItemQuantity;

/**
 * The ShopInterface class defines the functionality of the Shop feature.
 *
 * The Shop allows users to purchase new hair, top and pants items using the
 * game currency in the Balance class.
 *
 * @author: Anna Mae Decelo adecelo@myune.edu.au (Group: Just-A-Group)
 * @author: Alice Schofield aschof20@myune.edu.au (Group: Just-A-Group)
 */
public class ShopInterfaceController implements Initializable {

    @FXML
    public Button closeButton;
    public Button inventoryButton;

    // Instantiate the maps and inventory.
    private ShopMaps shopMap = new ShopMaps();
    private SpriteMaps spriteMap = new SpriteMaps();

    @FXML
    public TabPane femaleMenu;
    public TabPane maleMenu;

    @FXML
    public Tab invisibleTab1;
    public Tab invisibleTab2;
    public Tab invisibleTab3;
    public Tab invisibleTab4;

    @FXML
    public ImageView base_layer;
    public ImageView pants_layer;
    public ImageView top_layer;
    public ImageView hair_layer;
    public ImageView shop_top;
    public ImageView shop_pants;
    public ImageView shop_hair;

    private int[] userArray;

    // Objects for displaying and setting prices and currency
    @FXML
    public Text welcomeUserText;
    public Text hairPriceText;
    public Text topPriceText;
    public Text pantsPriceText;
    public Text totalPriceCopperText;
    public Text totalPriceSilverText;
    public Text totalPriceGoldText;
    public Text hairNameText;
    public Text topNameText;
    public Text pantsNameText;

    @FXML
    public ImageView hairCopperIcon;
    public ImageView hairSilverIcon;
    public ImageView hairGoldIcon;
    public ImageView topCopperIcon;
    public ImageView topSilverIcon;
    public ImageView topGoldIcon;
    public ImageView pantsCopperIcon;
    public ImageView pantsSilverIcon;
    public ImageView pantsGoldIcon;
    public ImageView backgroundImageView;

    @FXML
    public Text userCopperBalance;
    public Text userSilverBalance;
    public Text userGoldBalance;

    @FXML
    public DialogPane leaveShopDialog;

    public ShopInterfaceController() throws IOException {
    }

    /**
     * Automatically import sprite into the scene StackPanes.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Set starting icon and dialog visibility.
        setIconVisibility();

        // Reset the total price arrays.
        ShopPurchases.resetArrays();

        userArray = Sprite.retrieveCurrentSprite();
        if(userArray[0] == Gender.FEMALE.toInt()){

            importFeature(base_layer, spriteMap.getFemaleSkinMap(), Gender.FEMALE, 1, userArray[1]);

            //Import standard and shop pants layers.
            importFeature(pants_layer, spriteMap.getFemalePantsMap(),Gender.FEMALE,2, userArray[2]);
            importFeature(shop_pants, shopMap.getFemaleShopPants(),Gender.FEMALE,6, userArray[6]);

            //Import the standard and shop hair layers.
            importFeature(hair_layer, spriteMap.getFemaleHairMap(), Gender.FEMALE, 4,userArray[4]);
            importFeature(shop_hair, shopMap.getFemaleShopHair(), Gender.FEMALE, 7, userArray[7]);

            //Import standard and shop tops layers.
            importFeature(top_layer,spriteMap.getFemaleTopsMap(), Gender.FEMALE,3,(userArray[3]));
            importFeature(shop_top,shopMap.getFemaleShopTop(), Gender.FEMALE,5,(userArray[5]));

            // Hide male menu if sprite is female
            setMenuVisibility(true, false);


        } else  if(userArray[0] == Gender.MALE.toInt()){
            //updateSkinTone(spriteMap.getMaleSkinMap());
            importFeature(base_layer, spriteMap.getMaleSkinMap(), Gender.MALE, 1, userArray[1]);
            //Import standard and shop pants layers.
            importFeature(pants_layer, spriteMap.getMalePantsMap(), Gender.MALE, 2, userArray[2]);
            importFeature(shop_pants, shopMap.getMaleShopPants(), Gender.MALE, 6, userArray[6]);

            //Import standard and shop hair layer.
            importFeature(hair_layer, spriteMap.getMaleHairMap(), Gender.MALE, 4,userArray[4]);
            importFeature(shop_hair, shopMap.getMaleShopHair(), Gender.MALE, 7,userArray[7]);

            //Import standard and shop tops layers.
            importFeature(top_layer,spriteMap.getMaleTopsMap(), Gender.MALE,3,(userArray[3]));
            importFeature(shop_top,shopMap.getMaleShopTop(), Gender.MALE,5,(userArray[5]));

            // Hide female menu if sprite is male
            setMenuVisibility(false, true);
        }

        //From CODE MONKEYS
        try
        {
            Thread.sleep(500);

        } catch(InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        getUserBalance();

    }

    /************************** HAIR BUTTONS **************************/
    public void setFemaleHair1() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_1);}
    public void setFemaleHair2() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_2);}
    public void setFemaleHair3() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_3);}
    public void setFemaleHair4() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_4);}
    public void setFemaleHair5() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_5);}
    public void setFemaleHair6() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_6);}
    public void setFemaleHair7() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_7);}
    public void setFemaleHair8() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_8);}
    public void setFemaleHair9() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_9);}
    public void setFemaleHair10() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_10);}
    public void setFemaleHair11() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_11);}
    public void setFemaleHair12() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_12);}
    public void setFemaleHair13() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_13);}
    public void setFemaleHair14() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_14);}
    public void setFemaleHair15() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_15);}
    public void setFemaleHair16() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_16);}
    public void setFemaleHair17() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_17);}
    public void setFemaleHair18() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_18);}
    public void setFemaleHair19() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_19);}
    public void setFemaleHair20() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_20);}
    public void setFemaleHair21() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_21);}
    public void setFemaleHair22() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_22);}
    public void setFemaleHair23() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_23);}
    public void setFemaleHair24() { updateShopHair(Gender.FEMALE, ShopPrices.FEMALE_SHOP_HAIR_24);}
    
    public void setMaleHair1(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_1);}
    public void setMaleHair2(){ updateShopHair(Gender.MALE,ShopPrices.MALE_SHOP_HAIR_2);}
    public void setMaleHair3(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_3);}
    public void setMaleHair4(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_4);}
    public void setMaleHair5(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_5);}
    public void setMaleHair6(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_6);}
    public void setMaleHair7(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_7);}
    public void setMaleHair8(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_8);}
    public void setMaleHair9(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_9);}
    public void setMaleHair10(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_10);}
    public void setMaleHair11(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_11);}
    public void setMaleHair12(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_12);}
    public void setMaleHair13(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_13);}
    public void setMaleHair14(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_14);}
    public void setMaleHair15(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_15);}
    public void setMaleHair16(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_16);}
    public void setMaleHair17(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_17);}
    public void setMaleHair18(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_18);}
    public void setMaleHair19(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_19);}
    public void setMaleHair20(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_20);}
    public void setMaleHair21(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_21);}
    public void setMaleHair22(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_22);}
    public void setMaleHair23(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_23);}
    public void setMaleHair24(){ updateShopHair(Gender.MALE, ShopPrices.MALE_SHOP_HAIR_24);}

    /************************** TOP BUTTONS **************************/
    public void setFemaleTop1() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_1);}
    public void setFemaleTop2() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_2);}
    public void setFemaleTop3() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_3);}
    public void setFemaleTop4() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_4);}
    public void setFemaleTop5() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_5);}
    public void setFemaleTop6() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_6);}
    public void setFemaleTop7() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_7);}
    public void setFemaleTop8() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_8);}
    public void setFemaleTop10() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_10);}
    public void setFemaleTop11() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_11);}
    public void setFemaleTop12() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_12);}
    public void setFemaleTop13() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_13);}
    public void setFemaleTop14() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_14);}
    public void setFemaleTop15() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_15);}
    public void setFemaleTop16() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_16);}
    public void setFemaleTop17() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_17);}
    public void setFemaleTop18() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_18);}
    public void setFemaleTop19() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_19);}
    public void setFemaleTop20() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_20);}
    public void setFemaleTop21() { updateShopTop(Gender.FEMALE, ShopPrices.FEMALE_SHOP_TOP_21);}

    public void setMaleTop1() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_1);}
    public void setMaleTop2() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_2);}
    public void setMaleTop3() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_3);}
    public void setMaleTop4() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_4);}
    public void setMaleTop5() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_5);}
    public void setMaleTop7() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_7);}
    public void setMaleTop8() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_8);}
    public void setMaleTop9() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_9);}
    public void setMaleTop11() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_11);}
    public void setMaleTop12() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_12);}
    public void setMaleTop13() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_13);}
    public void setMaleTop14() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_14);}
    public void setMaleTop15() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_15);}
    public void setMaleTop16() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_16);}
    public void setMaleTop17() { updateShopTop(Gender.MALE, ShopPrices.MALE_SHOP_TOP_17);}

    /************************** PANTS BUTTONS **************************/
    public void setFemalePants1() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_1);}
    public void setFemalePants2() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_2);}
    public void setFemalePants3() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_3);}
    public void setFemalePants4() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_4);}
    public void setFemalePants5() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_5);}
    public void setFemalePants7() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_7);}
    public void setFemalePants8() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_8);}
    public void setFemalePants9() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_9);}
    public void setFemalePants10() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_10);}
    public void setFemalePants11() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_11);}
    public void setFemalePants12() { updateShopPants(Gender.FEMALE, ShopPrices.FEMALE_SHOP_PANTS_12);}

    public void setMalePants1() { updateShopPants(Gender.MALE, ShopPrices.MALE_SHOP_PANTS_1);}
    public void setMalePants2() { updateShopPants(Gender.MALE, ShopPrices.MALE_SHOP_PANTS_2);}
    public void setMalePants3() { updateShopPants(Gender.MALE, ShopPrices.MALE_SHOP_PANTS_3);}
    public void setMalePants4() { updateShopPants(Gender.MALE, ShopPrices.MALE_SHOP_PANTS_4);}
    public void setMalePants5() { updateShopPants(Gender.MALE, ShopPrices.MALE_SHOP_PANTS_5);}
    public void setMalePants8() { updateShopPants(Gender.MALE, ShopPrices.MALE_SHOP_PANTS_8);}
    public void setMalePants10() { updateShopPants(Gender.MALE, ShopPrices.MALE_SHOP_PANTS_10);}
    public void setMalePants12() { updateShopPants(Gender.MALE, ShopPrices.MALE_SHOP_PANTS_12);}

    /**
     * Function to set the visibility of menu items.
     * @param female - female menu visibility boolean.
     * @param male   - male menu visibility boolean.
     */
    private void setMenuVisibility(Boolean female, Boolean male){
        femaleMenu.setVisible(female);
        maleMenu.setVisible(male);
    }

    /**
     * Function to update standard items to blank when shop items are purchased.
     * @param gender    - sprite gender.
     * @param layer     - stack layer to be updated.
     * @param map       - map where the replacement image is retrieved from.
     * @param key       - key in the userArray to be updated.
     * @param blank     - value to update the userArray key.
     */
    private void updateStandard(Gender gender, ImageView layer, Map<Integer, Image> map, Integer key, Integer blank){
        if(gender.toInt() == Gender.MALE.toInt()){
            layer.setImage(map.get(blank));
            userArray[key] = blank;
        } else if (gender.toInt() == Gender.FEMALE.toInt()){
            layer.setImage(map.get(blank));
            userArray[key] = blank;
        }
    }

    /**
     * Function to import features from the current character.
     * @param imageView - stackpane layer to be updated.
     * @param map       - map location of the feature image.
     * @param gender    - gender of the sprite
     * @param key       - userArray key of the element.
     * @param value     - image of the userArray element.
     */
    private void importFeature(ImageView imageView, Map<Integer, Image> map, Gender gender, int key, int value) {
        if (userArray[0] == gender.toInt() && gender.toInt() == Gender.FEMALE.toInt()) {
            drawImage(imageView, map,key, value );
        } else if(userArray[0] == gender.toInt() && gender.toInt() == Gender.MALE.toInt()){
            drawImage(imageView, map,key, value );
        }
    }

    /**
     * Function to update the shop hair to the user's selection and set the standard hair to blank.
     * @param gender    - gender of the sprite.
     * @param index     - index of the users shop hair selection.
     */
    private void updateShopHair(Gender gender, ShopPrices index) {
        if (userArray[0] == gender.toInt() && gender.toInt() == Gender.FEMALE.toInt()) {
            ShopPurchases.updateHairPrice(index, hairPriceText, totalPriceCopperText, totalPriceSilverText,
                                        totalPriceGoldText, hairNameText, hairCopperIcon, hairSilverIcon, hairGoldIcon);
            updateStandard(gender, hair_layer, spriteMap.getFemaleHairMap(), 4,Hair.FEMALE_BLANK.toInt());
            drawImage(shop_hair, shopMap.getFemaleShopHair(), 7, index.toInt());
        }
        if (userArray[0] == gender.toInt() && gender.toInt() == Gender.MALE.toInt()){
            ShopPurchases.updateHairPrice(index, hairPriceText, totalPriceCopperText, totalPriceSilverText,
                                        totalPriceGoldText, hairNameText, hairCopperIcon, hairSilverIcon, hairGoldIcon);
            updateStandard(gender, hair_layer, spriteMap.getMaleHairMap(), 4,Hair.MALE_BLANK.toInt());
            drawImage(shop_hair, shopMap.getMaleShopHair(), 7, index.toInt());
        }
    }

    /**
     * Function to update the shop top.
     * @param gender - sprite gender.
     * @param index  - index of the selected top.
     */
    private void updateShopTop(Gender gender, ShopPrices index) {
        if (userArray[0] == gender.toInt() && gender.toInt() == Gender.FEMALE.toInt()) {
            ShopPurchases.updateTopPrice(index, topPriceText, totalPriceCopperText, totalPriceSilverText,
                                        totalPriceGoldText, topNameText, topCopperIcon, topSilverIcon, topGoldIcon);
            updateStandard(gender, top_layer, spriteMap.getFemaleTopsMap(), 3, Tops.FEMALE_BLANK.toInt());
            drawImage(shop_top, shopMap.getFemaleShopTop(), 5, index.toInt());
        }
        if (userArray[0] == gender.toInt() && gender.toInt() == Gender.MALE.toInt()){
            ShopPurchases.updateTopPrice(index, topPriceText, totalPriceCopperText, totalPriceSilverText,
                                        totalPriceGoldText, topNameText, topCopperIcon, topSilverIcon, topGoldIcon);
            updateStandard(gender, top_layer, spriteMap.getMaleTopsMap(), 3, Tops.MALE_BLANK.toInt());
            drawImage(shop_top, shopMap.getMaleShopTop(), 5, index.toInt());
        }
    }

    /**
     * Function to update the shop pants.
     * @param gender    - gender of the sprite.
     * @param index     - index of the shop pants selected.
     */
    private void updateShopPants(Gender gender, ShopPrices index) {
        if (userArray[0] == gender.toInt() && gender.toInt() == Gender.FEMALE.toInt()) {
            ShopPurchases.updatePantsPrice(index, pantsPriceText, totalPriceCopperText, totalPriceSilverText,
                                        totalPriceGoldText, pantsNameText, pantsCopperIcon, pantsSilverIcon, pantsGoldIcon);
            updateStandard(gender, pants_layer, spriteMap.getFemalePantsMap(), 2,Pants.FEMALE_BLANK.toInt());
            drawImage(shop_pants, shopMap.getFemaleShopPants(), 6, index.toInt());
        }
        if (userArray[0] == gender.toInt() && gender.toInt() == Gender.MALE.toInt()){
            ShopPurchases.updatePantsPrice(index, pantsPriceText, totalPriceCopperText, totalPriceSilverText,
                                        totalPriceGoldText, pantsNameText, pantsCopperIcon, pantsSilverIcon, pantsGoldIcon);
            updateStandard(gender, pants_layer, spriteMap.getMalePantsMap(), 2,Pants.MALE_BLANK.toInt());
            drawImage(shop_pants, shopMap.getMaleShopPants(), 6, index.toInt());
        }
    }

    /**
     * Function to generate the sprite and validate shop purchases against
     * user balance.
     * @throws IOException - thrown if files to build stack unavailable.
     */
    public void buildSpriteCharacter() throws IOException {
        /* Function to generate the shop sprite.
        * Note: the currency was not fully implemented therefore this feature
        * could not be complete.
        * */
        generateSprite();

        int userCopper = Integer.parseInt(userCopperBalance.getText());
        int userSilver = Integer.parseInt(userSilverBalance.getText());
        int userGold = Integer.parseInt(userGoldBalance.getText());

        boolean valid = true;
        boolean fundsAvailable = false;

        // If not enough copper, take 1 from the silver currency and
        // convert to 100 copper if enough silver.
        if (!enoughCopper(userCopper)) {
            if (userSilver > 0) {
                userSilver -= 1;
                userCopper += 100;
                fundsAvailable = true;
            }

            // If not enough silver, take 1 from gold currency and convert
            // to 99 silver and 100 copper.
            else if (userGold > 0) {
                userGold -= 1;
                userSilver += 99;
                userCopper += 100;
                fundsAvailable = true;
            }

            else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Insufficient funds!");
                alert.showAndWait();
                valid = false;
            }
        }

        if (valid) {
            if (!enoughSilver(userSilver)) {

                // If not enough silver, take 1 from the gold currency and
                // convert to 100 silver if enough gold.
                if (userGold > 0) {
                    userGold -= 1;
                    userSilver += 100;
                    fundsAvailable = true;
                }

                else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setContentText("Insufficient funds!");
                    alert.showAndWait();
                    valid = false;
                }
            }
        }

        if (valid) {
            // If not enough gold, bring up error message.
            if (!enoughGold(userGold)) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Insufficient funds!");
                alert.showAndWait();
            } else {
                fundsAvailable = true;
            }
        }

        // If funds are enough, deduct the total price from the user balance,
        // generate the sprite, add items to inventory, and update currency in inventory.
        if (fundsAvailable) {
            addToInventory();

            int deductCopper = Integer.parseInt(totalPriceCopperText.getText());
            int deductSilver = Integer.parseInt(totalPriceSilverText.getText());
            int deductGold = Integer.parseInt(totalPriceGoldText.getText());

            userCopper -= deductCopper;
            userCopperBalance.setText(String.valueOf(userCopper));
            InventoryTable.addInventory(MuckClient.INSTANCE.getId().toString(), "Copper Coins",
                            "Currency", -deductCopper, 0, 0);

            userSilver -= deductSilver;
            userSilverBalance.setText(String.valueOf(userSilver));
            InventoryTable.addInventory(MuckClient.INSTANCE.getId().toString(), "Silver Coins",
                    "Currency", -deductSilver, 0, 0);

            userGold -= deductGold;
            userGoldBalance.setText(String.valueOf(userGold));
            InventoryTable.addInventory(MuckClient.INSTANCE.getId().toString(), "Gold Coins",
                    "Currency", -deductGold, 0, 0);
        }
    }

    /**
     * Function to render the single sprite and sprite sheets.
     * @throws IOException - files used to build stack not available.
     */
    private void generateSprite() throws IOException {
        Sprite sprite = new Sprite(userArray[0], userArray[1], userArray[2], userArray[3], userArray[4], userArray[5],
                userArray[6], userArray[7]);
        // Call the function to create file name from the keys of the features
        String render = Sprite.addSpriteToDatabase(userArray); //Create a string from the array
        //update profile
        MuckClient client = MuckClient.getINSTANCE();
        String code = render.substring(0, render.length() - 4);
        client.send(new RequestUpdateSprite(client.getId(), code));
        //generate image
        sprite.generateSpritesheet(render);
        sprite.generateSprite(render);
    }

    /**
     * Function to show the leave shop dialog pane.
     */
    public void toWorldButton() {
        leaveShopDialog.setVisible(true);
    }

    /**
     * Function to return to the canvas after purchase.
     * @throws IOException - thrown if .fxml file not available.
     */
    public void backToTownButton() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/appScreen.fxml"));
        fxmlLoader.setLocation(getClass().getResource("/appScreen.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root, App.primaryStage.getWidth(), App.primaryStage.getHeight());
        App.primaryStage.setScene(scene);
        App.primaryStage.show();
    }

    /**
     * Function to close the leave shop dialog pane.
     */
    public void closeDialogButton() {
        leaveShopDialog.setVisible(false);
     }

    /**
     * Function to draw images to layers.
     * @param imageView - stackpane layer to be drawn.
     * @param map       - map to retrieve layer image from.
     * @param key       - key of the userArray updated.
     * @param value     - image of the key.
     */
    private void drawImage(ImageView imageView, Map<Integer, Image> map, int key, int value){
        Image img = map.get(value);
        imageView.setImage(img);
        userArray[key] = value;
    }

    /**
     * Function to dynamically set visibility of elements in the UI.
     */
    private void setIconVisibility() {
        invisibleTab1.setDisable(true);
        invisibleTab2.setDisable(true);
        invisibleTab3.setDisable(true);
        invisibleTab4.setDisable(true);

        hairCopperIcon.setVisible(true);
        hairSilverIcon.setVisible(false);
        hairGoldIcon.setVisible(false);

        topCopperIcon.setVisible(true);
        topSilverIcon.setVisible(false);
        topGoldIcon.setVisible(false);

        pantsCopperIcon.setVisible(true);
        pantsSilverIcon.setVisible(false);
        pantsGoldIcon.setVisible(false);

        leaveShopDialog.setVisible(false);
    }


    /*  Currency related functions */

    /**
     * Function to get the users current balance.
     */
    private void getUserBalance() {
        //From CODE MONKEYS
        getInventory(MuckClient.INSTANCE.getId().toString());  //calls returnInventory

        //From CODE MONKEYS
        int copperInventoryBalance = getInventoryItemQuantity("Copper Coins");
        int silverInventoryBalance = getInventoryItemQuantity("Silver Coins");
        int goldInventoryBalance =getInventoryItemQuantity("Gold Coins");

        displayBalance(copperInventoryBalance, silverInventoryBalance, goldInventoryBalance);

        if (copperInventoryBalance == 0 && silverInventoryBalance == 0 && goldInventoryBalance == 0) {
            System.out.println("Error while loading inventory database.");

            Balance userBalance = new Balance (new int[] {60, 80, 99});

            int copperBalance = userBalance.GetCurrency(GameCurrency.valueOf(0));
            int silverBalance = userBalance.GetCurrency(GameCurrency.valueOf(1));
            int goldBalance = userBalance.GetCurrency(GameCurrency.valueOf(2));

            displayBalance(copperBalance, silverBalance, goldBalance);
        }
    }

    /**
     * Function to render the users balance.
     * @param copper - copper balance.
     * @param silver - silver balance.
     * @param gold   - gold balance.
     */
    private void displayBalance(int copper, int silver, int gold) {
        //Display current user balance.
        userCopperBalance.setText(Integer.toString(copper));
        userSilverBalance.setText(Integer.toString(silver));
        userGoldBalance.setText(Integer.toString(gold));
    }

    /**
     * Function to open inventory from the shop.
     * @throws IOException - thrown if .fxml file not available.
     */
    public void openInventory() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Inventory/UserInventoryTable.fxml"));
        fxmlLoader.setLocation(getClass().getResource("/Inventory/UserInventoryTable.fxml"));
        Parent root = fxmlLoader.load();
        Stage inventoryStage = new Stage();
        Scene inventoryScene = new Scene(root);
        inventoryStage.setScene(inventoryScene);
        inventoryStage.setTitle("User Inventory System"); //shows at top of window
        inventoryStage.initModality(Modality.APPLICATION_MODAL);
        inventoryStage.show();
    }

    /**
     * Function to add purchases to the inventory.
     */
    private void addToInventory() {
        if (!hairNameText.getText().equals("Selected Hair")) {
            InventoryTable.addInventory(MuckClient.INSTANCE.getId().toString(), hairNameText.getText(),
                    "Hair", 1, 1, 1);
            System.out.println("Added hair to inventory");
        }

        if (!topNameText.getText().equals("Selected Top")) {
            InventoryTable.addInventory(MuckClient.INSTANCE.getId().toString(), topNameText.getText(),
                    "Tops", 1, 1, 1);
            System.out.println("Added top to inventory");
        }

        if (!pantsNameText.getText().equals("Selected Pants")) {
            InventoryTable.addInventory(MuckClient.INSTANCE.getId().toString(), pantsNameText.getText(),
                    "Pants", 1, 1, 1);
            System.out.println("Added pants to inventory");
        }
    }

    /**
     * Function to validate copper purchase.
     * @param userCopper - amount of copper coins.
     * @return - boolean is balance is greater than the purchase.
     */
    private boolean enoughCopper(int userCopper) {
        return userCopper >=  Integer.parseInt(totalPriceCopperText.getText());
    }

    /**
     * Function to validate silver purchase.
     * @param userSilver - amount of silver coins.
     * @return - boolean is balance is greater than the purchase.
     */
    private boolean enoughSilver(int userSilver) {
        return userSilver >=  Integer.parseInt(totalPriceSilverText.getText());
    }


    /**
     * Function to validate gold purchase.
     * @param userGold - amount of silver coins.
     * @return - boolean is balance is greater than the purchase.
     */
    private boolean enoughGold(int userGold) {
        return userGold >=  Integer.parseInt(totalPriceGoldText.getText());
    }



}
